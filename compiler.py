import os
from shutil import copyfile
import re
from  datetime import datetime as dt
import shutil


 I broke this code by recklessly deleteing things
 
 
'''
   </div>
</div>

<div class="row">
  <div class="column leftside">
        <div class="sidenavbox">
              ###sidenav####
        </div>
  </div>
  <div class="column middle">

     ###variable-key###
     <h1>###shorts:title###</h1>
     ###body###
  </div>
  <div class="column rightside">
<!----     <p>space for google ads here</p> ---->
  </div>
</div>

<div class="sign">
<sign>&copy; MC Byington</sign>
</div>

<p></p>

</body>
</html>'''

class Article(object):
    def __init__(self,source,path):
        self.path = path
        filepath = source+path if source.endswith('/') else source+'/'+path

        stkeys = ['###Head', '###Body', '###Variable-table']
        storage = dict(zip(stkeys,['' for i in stkeys]))
        active = dict(zip(stkeys,[False for i in stkeys]))
        stpkeys = dict(zip([s[:3]+'/'+s[3:] for s in stkeys],[s for s in stkeys]))

        with open(filepath) as srcs:
           for line in srcs:
               if line.strip() != '':
                   if line.strip() in stpkeys:
                       active[stpkeys[line.strip()]] = False
                   for k in [i for i in active.keys() if active[i]]:
                       storage[k] += line
                   if line.strip() in stkeys:
                       active[line.strip()] = True
        self.head = storage['###Head']
        self.body = storage['###Body']
        self.var_table = storage['###Variable-table']
        self.shortkeys(storage['###Head'])
        self.date_dt = dt.strptime(self.date, "%Y-%m-%d")
    def shortkeys(self,strings):
        mt = []
        for l in strings.split('\n'):
            if l.strip() != '':
                a,b = l.strip().split(':')
                b = [i.strip() for i in b.split(';')] if ';' in b else b.strip()
                mt.append( (a.strip(),b))
        self.shorts = dict(mt)
        for key in dict(mt):
            setattr(self, key, dict(mt)[key])

    def htmlpath(self):
        return self.path.rstrip('article')+'html'
    def rel_basepath(self):
        return '../'*(len(self.path.split('/'))-1)
    def make_var_table(self):

        java = '''<script>
function ButtonVanish() {
  var x = document.getElementById("variable-key-outer");
  var y = document.getElementById("variable-key-button");
   if (x.style.display == "block"){
       x.style.display = "none";
       y.textContent = "Show variable key";}
   else{
       x.style.display = "block";
       y.textContent = "Hide variable key";
   }
}
</script>



<div id="variable-key-div" >
<button onclick="ButtonVanish()" id="variable-key-button" class="variable-button" >Show variable key</button>
</div>



'''
        if self.var_table.strip() != '':
            return java+ ' <p></p>\n <div id="variable-key-outer"> <div id="variable-key-mid">   <table id="variable-key"> <tbody>\n'+ self.var_table +'\n</tbody>\n</table> </div></div>'
        else:
            return ''

def recreater(source,output,convertsvgs=True, jaxupdate=False):
    output = output if output.endswith('/') else output+'/'
    for j in os.listdir(output):
        if 'MathJax' not in j and not jaxupdate:
            if os.path.isdir(output+j):
                shutil.rmtree(output+j)
            else:
                os.remove(output+j)
  #  a = input('look at output tree')
   # os.makedirs(output)

    articles = []

    def recurse(source,path):
        path = path if path.endswith('/') or path=='' else path +'/'
        source = source if source.endswith('/') else source+'/'
        for j in os.listdir(source+path):
            if os.path.isdir(source+path+j):
                if not os.path.isdir(output+path+j):
                    os.makedirs(output+path+j)
                recurse(source,path+j)
            elif j.endswith('.svg') and convertsvgs:
                if not os.path.isdir(output+path):
                    os.makedirs(output+path)
                os.system('inkscape --file=%s  --without-gui --export-text-to-path  --export-plain-svg=%s'%(source+path+j,output+path+j))
            elif '-draft' in j and convertsvgs:
                print Article(source,path+j).title
#                continue
            elif '-trash' in j:
                continue
            elif j.endswith('.article'):
                articles.append(Article(source,path+j))
            elif "MathJax" not in j or jaxupdate:
                if os.path.isdir(output+path):
                    copyfile(source+path+j, output+path+j)
                else:
                    os.makedirs(output+path)
                    copyfile(source+path+j, output+path+j)

    recurse(source,'')
    return articles


def make_series_nav(art,articles):
    series = art.series_name
    out = '<ul>\n <li class="sidenavlink"> <p class="series_head"> %s </p></li>\n'%short_headings[art.series_name]
    for a in sorted([j for j in articles if j.series_name==series],key=lambda x: float(x.series_order) ):
        link = art.rel_basepath()
        link += a.htmlpath()
        if art.series_order != a.series_order:
            out += '<li class="sidenavlink"><a href="%s"> %s </a></li>\n'%(link,a.short_title)
        else:
            out += '<li class ="sidenavlink"><a class="active" href="%s"> %s </a></li>\n'%(link,a.short_title)
    out+= '</ul>\n'
    return out
def make_biglink(alink,art_current):
    blank = '''<a href="###link###"> <div id = "lisal">
                <div id = "date-block">
                        <div id = "date-line">  <p class="all-post"> ###date### <span id="in-series"> in ###series### </span> </p> </div>
                   </div>
                   <div id = "all-post-title">
                           <p class="all-post">###title###</p>
                   </div>
               </div></a>\n\n'''
    blank = blank.replace('###title###',alink.title)
    blank = blank.replace('###date###',alink.date)
    blank = blank.replace('###link###',art_current.rel_basepath()+alink.htmlpath() )
    blank = blank.replace('###series###',dict(series_headings)[alink.series_name] )
    return blank

def check_body_links(art,articles):
    bd = ''
    for line in art.body.split('\n'):
        line = line.replace('<p>', '<p class="article">')
        mt = re.search('##link##.*###',line)
        if mt:
            key,value = [j.strip() for j in line[mt.start()+8:mt.end()-3].split(':')]
            link = [k for k in articles if k.shorts[key]==value]
            if len(link) != 1:
                raise Exception( "article %s,link (%s) matches %.0f articles "%(art.title,line[mt.start()+8:mt.end()-3],len(link)))
            link=link[0]
            line = line[:mt.start()]+'"%s%s"' %(art.rel_basepath(),link.htmlpath())+ line[mt.end():]

        mt = re.search('##biglink##.*###',line)
        if mt:
            key,value = [j.strip() for j in line[mt.start():mt.end()].split('##')[2].split(':')]
            link = [k for k in articles if k.shorts[key]==value]


            if len(link) != 1:
                raise Exception( "link (%s) matches %.0f articles"%(value,len(link)))
            link=link[0]
            line = line[:mt.start()] + make_biglink(link,art)+ line[mt.end():]


        capkey = '<p class="caption">'
        mt = re.search(capkey,line)
        cap = '<span class="caption"> [Caption]</span>'
        if mt:
            line = line.replace(capkey, capkey+cap)
        bd+= line + '\n'
    return bd

def make_head_links(series_headings,current,articles):
    links = ''
    for key,hd in series_headings:
        arts = [j for j in articles if j.series_name == key]
        arts = sorted(arts, key=lambda x: x.series_order)
        art = arts[0]
        if art.series_name == current.series_name:
            s = '<a class="active" href="'
        else:
            s = '<a href="'

        links+= s + current.rel_basepath() + art.htmlpath() + '">' + hd+'</a >\n'
    return links


def make_new_art(art,articles,outfilepath,template):
    def make_links(art, skey = lambda x: x.date_dt):
        ll = ''
        for a in sorted(articles,key = skey):
            if a.series_name != 'home' and a.series_name != 'contact':
                ll += make_biglink(a,art)
        return ll
    output = template
    output = output.replace(r'###basepath###',art.rel_basepath())
    output = output.replace(r'###self:link###',art.htmlpath())
    output = output.replace(r'###shorts:title###',art.title)
    output = output.replace(r'###shorts:content###',art.description)
    output = output.replace(r'###shorts:author###',str(art.author))
    heads = make_head_links(series_headings,art,articles)
    output = output.replace(r'###headlinks###',heads)
    output = output.replace(r'###sidenav####', make_series_nav(art,articles))
    output = output.replace(r'###body###',check_body_links(art,articles))
    output = output.replace(r'###variable-key###',art.make_var_table())

    if '###Art-list-recent###' in output:
        output = output.replace(r'###Art-list-recent###', make_links(art,skey = lambda x: -int(x.date_dt.strftime('%s'))))
    if '###Art-list-beginning###' in output:
        output = output.replace(r'###Art-list-beginning###', make_links(art))
    with open(outfilepath+art.htmlpath(), 'w') as outfile:
         outfile.write(output)


def write_all(articles,output,forserver=True):
    if forserver:
        t = template1+mj_insert+template2
    else:
        t = template1+mj_server+template2
    for a in articles:
       print a.path
       make_new_art(a,articles,output,t)


source = '/home/mbyington/Financial_writing/web_html/source'
output = '/home/mbyington/Financial_writing/web_html/output/'

series_headings = [('Loans','Loans'),
                   ('FIRE','Financial independence and investing'),
                   ('COL','Lifestyle analysis'),
                   ('accounting','Accounting'),
                   ('about','About'),
                   ('contact','Contact')]
short_headings = {'about':'About', 'FIRE':'FIRE and investing articles',
                  'contact':'Contact', 'accounting':'Accounting articles',
                  'Loans':'Loan articles', 'COL':'Lifestyle analysis articles',
                  'home':'Home'}




##### recreater runs slow (about 10 sec) if convertsvgs=True
##### if convertsvgs, recreater converts all svg text to paths before moving them to new folders
#####

#      source: source directory for .article files
#
#      output: output directory for .html files
#
#      convertsvgs=True (default): converts svg text to paths; massive increase in run time (over 10sec)
#
#      jaxupdate=False: updates the MathJax library from whatever is saved in /source otherwise does not delete MathJax from output



T = True
F = False
forserver=T


articles = recreater(source,output,convertsvgs=forserver)
write_all(articles,output,forserver)

real = [ a for a in articles if a.series_name != 'home' and a.series_name != 'contact']

if forserver:
    os.system('rsync -av --delete  /home/mbyington/Financial_writing/web_html/output/*  michael@wwwv4.domestic-engineering.com:/home/httpd/html/domestic')
    os.system('rsync -av --delete  /home/mbyington/Financial_writing/web_html/output/*  michael@wwwv6.domestic-engineering.com:/home/httpd/html/domestic')
#    os.system('rsync -av --delete --ipv6 /home/mbyington/Financial_writing/web_html/output/*  michael@www.domestic-engineering.com:/home/httpd/html/domestic')




for a in articles:
    if a.series_name != 'home' and a.series_name != 'contact': print "%s: %s"%(short_headings[a.series_name],a.title)


for key in short_headings:
    if key != 'contact' and key != 'home':
        sub = [ a for a in real if a.series_name == key]
        print (short_headings[key],len(sub))

print len(real)
